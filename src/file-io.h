#ifndef _FILE_IO_H
#define _FILE_IO_H

#include <string>
#include <map>

typedef unsigned char byte;

template<typename T> class Array
{
    bool _deleteOnDestruct;
public:
    Array() : count(0), data(nullptr), _deleteOnDestruct(false) { }
    Array(int count) : _deleteOnDestruct(true) { this->Allocate(count); }
    Array(int count, T* data) : count(count), data(data), _deleteOnDestruct(false) { }
    virtual ~Array() { if (this->_deleteOnDestruct) this->Delete(); }

    int count;
    T* data;

    operator T*(void) const { return data; }
    const T& operator[] (int index) const { return this->data[index]; }
    T& operator[] (int index) { return this->data[index]; }

    virtual void Allocate(int count)
    {
        this->count = count;
        this->data = this->count > 0 ? new T[this->count] : nullptr;
    }

    void Map(int count, T* data)
    {
        this->count = count;
        this->data = data;
    }

    virtual void Delete()
    {
        if (this->data != nullptr) delete []this->data;
        this->data = nullptr;
        this->count = 0;
    }
};

typedef std::string (DataFileLocator)(const std::string& relativeFilename);
typedef Array<byte>& (DataFileLoader)(const std::string& filename);

class FileData : public Array<byte>
{
public:
    std::string _filename;
};

typedef std::map<std::string, FileData> LoadedFileList;

class FileIO
{
    static LoadedFileList _loadedFiles;
public:
    static std::string LocateDataFile(const std::string& filename);
    static Array<byte>& LoadFileData(const std::string& filename);
    static Array<byte> LoadPartialFileData(const std::string& filename, int count);

private:
    static void UnloadFileData(FileData& fileData);
};

#endif // _FILE_IO_H
