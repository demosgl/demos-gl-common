#include "file-io.h"
#include <fstream>

LoadedFileList FileIO::_loadedFiles;

std::string FileIO::LocateDataFile(const std::string& filename)
{
    return filename;
}

Array<byte>& FileIO::LoadFileData(const std::string& filename)
{
    if (FileIO::_loadedFiles.find(filename) == FileIO::_loadedFiles.end())
    {
        std::ifstream file(filename, std::ios::in | std::ios::binary | std::ios::ate);

        if (file.is_open())
        {
            if (file.tellg() > 0)
            {
                FileData data;
                data._filename = filename;
                data.Allocate(file.tellg());
                file.seekg(0, std::ios::beg);
                file.read((char*)data.data, data.count);
                FileIO::_loadedFiles.insert(std::make_pair(filename, data));
            }
            file.close();
        }
        else
            throw std::runtime_error("File not found");
    }

    return FileIO::_loadedFiles[filename];
}

Array<byte> FileIO::LoadPartialFileData(const std::string& filename, int count)
{
    FileData data;
    std::ifstream file(filename, std::ios::in | std::ios::binary | std::ios::ate);

    if (file.is_open())
    {
        if (file.tellg() >= count)
        {
            data._filename = filename;
            data.Allocate(count);
            file.seekg(0, std::ios::beg);
            file.read((char*)data.data, data.count);
        }
        file.close();
    }
    else
        throw std::runtime_error("File not found");

    return data;
}

void FileIO::UnloadFileData(FileData& fileData)
{
    FileIO::_loadedFiles.erase(fileData._filename);
}
