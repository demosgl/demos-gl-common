
#define GLEXTL_IMPLEMENTATION
#include <GL/glextl.h>

#include "program.h"
#include <iostream>
#include <GLFW/glfw3.h>

int Program::Run(int argc, char* argv[])
{
    this->SetArguments(argc, argv);
    
    if (glfwInit())
    {
        GLFWwindow* window = glfwCreateWindow(this->Width(), this->Height(), this->Title(), NULL, NULL);
        if (window == 0)
        {
            glfwTerminate();
            return 0;
        }

        glfwMakeContextCurrent(window);

        if (glExtLoadAll((PFNGLGETPROC*)&glfwGetProcAddress) == GL_FALSE)
        {
            glfwTerminate();
            return 0;
        }

        if (this->Initialize())
        {
            int width, height;
            while (glfwWindowShouldClose(window) == false)
            {
                glfwGetFramebufferSize(window, &width, &height);
                this->Render(glfwGetTime(), width, height);

                glfwSwapBuffers(window);
                glfwPollEvents();
            }
            this->Destroy();
        }

        glfwDestroyWindow(window);
        glfwTerminate();
    }
    return 0;
}
