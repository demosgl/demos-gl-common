#ifndef PROGRAM_H
#define PROGRAM_H

#include <vector>
#include <string>

class Program
{
    const char* _title;
    int _width;
    int _height;
    std::vector<std::string> _args;
public:
    const char* Title() { return this->_title; }
    int Width() const { return this->_width; }
    int Height() const { return this->_height; }
    std::vector<std::string> Arguments() { return this->_args; }
    const std::string& Argument(int index) const { return this->_args[index]; }

protected:
    Program() : _title("demos.gl"), _width(800), _height(600) { }
    Program(const char* title) : _title(title), _width(800), _height(600) { }
    Program(const char* title, int width, int height) : _title(title), _width(width), _height(height) { }
    virtual ~Program() { }
    
    void SetArguments(int argc, char* argv[]) { for (int i = 1; i < argc; ++i) this->_args.push_back(argv[i]); }

    virtual bool Initialize() { return true; }
    virtual void Render(float time, int width, int height) { }
    virtual void Destroy() { }

public:
    int Run(int argc = 0, char* argv[] = 0);

};

#endif // PROGRAM_H
